const express = require("express")
const axios = require("axios")
 
const app = express()

var cors = require('cors');

app.use(cors());


// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded());

// Parse JSON bodies (as sent by API clients)
app.use(express.json());
 
app.get("/", (req, res) => {
  res.send("Welcome to your App!")
})
 
app.get("/getData", (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');
  axios.get("https://jsonplaceholder.typicode.com/posts")
    .then(function(response) {
      res.json(response.data)
    }).catch(function(error) {
      res.json("Error occured!")
    })
})

app.post("/putToken", (req, res) => {
  return res.send(req.headers.authorization);
});

app.post("/muchote", (req, res, param) => {
	return param; 
});
 
app.listen(5000, function () {
  console.log(`Express server listening on port`)
})